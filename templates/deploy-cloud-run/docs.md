# deploy-cloud-run

**Status: [Beta][beta]**

> - `google_cloud_support_feature_flag` (Beta) flag need to be enabled to use the Component.

The `deploy-cloud-run` GitLab Component automates the deployment of your Cloud Run services within your GitLab CI/CD pipeline. It offers flexible deployment behavior,
creating a brand-new service if it doesn't already exist in your project and region. Conversely, if a Cloud Run service with the same name is already present, the component efficiently updates it with
your setup.

## Prerequisites

- Google Cloud Workload Identity Federation should be set up with [GitLab - Google Cloud integration onboarding process][onboarding].
- The Workload Identity used to create the release should have proper permissions configured. Please check [Authorization](#authorization) section.

## Usage
### Use as a CI/CD component
Add the following to your `gitlab-ci.yml`:
``` yaml
include:
  - component: gitlab.com/google-gitlab-components/cloud-run/deploy-cloud-run@{VERSION}
    inputs:
      project_id: "my-project-id"
      service: "my-service-name"
      region: "us-central1"
      image: "gcr.io/cloudrun/hello:latest"
```

## Authentication

- The component authenticates to Google Cloud services by Workload Identity Federation. Please follow [Gitlab - Google Cloud integration onboarding process][onboarding] to complete the Workload Identity Federation setup.

> - Requires `google_cloud_support_feature_flag` (Beta) flag need to be enabled.

## Inputs

| Input                        | Description                                                                  | Example           | Default Value      |
|------------------------------|------------------------------------------------------------------------------|-------------------|--------------------|
| `project_id`                 | (Required) The ID of the Google Cloud project in which to deploy the service | `my-project`      |                    |
| `region`                     | (Required) Region in which to deploy the service                             | `us-central1`     |                    |
| `service`                    | (Required) ID of the service or fully-qualified identifier of the service    | `my-service-name` |                    |
| `image`                      | (Required) Fully-qualified name of the container image to deploy             | `gcr.io/cloudrun/hello`                 |                    |
| `stage`                      | (Optional) The GitLab CI/CD stage                                            | `my-stage`        | `deploy`           |
| `as`                         | (Optional) The name of the job to execute                                    | `my-job-name`     | `deploy-cloud-run` |


## Authorization

To use the Component, the provided Workload Identity should have the following minimum roles:
- Cloud Storage Admin (`roles/run.admin`)
  - Can get,create and update services
  - Can get and set iam policies 
- Service Account User (`roles/iam.serviceAccountUser`)
  - Attach a service account to Cloud Run service

[beta]: https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta
[onboarding]: https://docs.gitlab.com/ee/integration/google_cloud_iam.html